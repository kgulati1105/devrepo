﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Assignment.Web.Models;
using Assignment.Services;
using Microsoft.AspNetCore.Http;
using Assignment.Web.DataAccess.Models;
using Microsoft.Extensions.Configuration;

namespace Assignment.Web.Controllers
{
    public class LoginController : Controller
    {
        private readonly ILogger<LoginController> _logger;
        private readonly IUserService _userService;
        private readonly IEmailService _emailService;
        private readonly IConfiguration _configuration;

        public LoginController(ILogger<LoginController> logger, IUserService userService, IEmailService emailService, IConfiguration configuration)
        {
            _logger = logger;
            _userService = userService;
            _emailService = emailService;
            _configuration = configuration;
        }

        /// <summary>
        /// It redirects to login page
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


        /// <summary>
        /// This method authenticates user
        /// </summary>
        /// <param name="userLogin"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UserLogin(UserLogin userLogin)
        {
            if (_userService.IsUserExist(userLogin.EmailAddress))
            {
                if (_userService.IsUserActive(userLogin.EmailAddress))
                {
                    var encryptedPassword = UserController.EncryptString(userLogin.Password);
                    var result = _userService.AuthenticateUser(userLogin.EmailAddress, encryptedPassword);
                    if (result != null)
                    {
                        HttpContext.Session.SetString("UserName", userLogin.EmailAddress);
                        return RedirectToAction("Welcome","User", result);
                    }
                    else
                    {
                        ModelState.AddModelError(nameof(userLogin.Password), "Incorrect user name or password.");
                        return View("Index");
                    }
                }
                else
                {
                    ModelState.AddModelError(nameof(userLogin.EmailAddress), "Account has not been activated.");
                    return View("Index");
                }
            }
            else
            {
                ModelState.AddModelError(nameof(userLogin.EmailAddress), "User is Not Found.");
                return View("Index");
            }
        }


       
    }
}
