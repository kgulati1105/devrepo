﻿
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
namespace Assignment.Web.DataAccess.Models
{ 
    public class UserDetails
    {

        [Required]
        [StringLength(50, ErrorMessage ="Character should not exceed more than 50.")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(60, ErrorMessage = "Character should not exceed more than 50.")]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Remote("IsValidDateOfBirth","User", HttpMethod = "Post", ErrorMessage = "Please provide a valid date of birth.")]
        public DateTime DateOFBirth { get; set; }

        [Required]
        public string Gender { get; set; }

        [Required]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage ="Please enter valid email address.")]
        public string EmailAddress { get; set; }

        public string ActivationLink { get; set; }

        public bool IsActive { get; set; }

        public bool IsPasswordGenerated { get; set; }

        public string Password { get; set; }
    }

}
