﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Assignment.Web.DataAccess.Models
{
    public class UserLogin
    {
        [NotMapped]
        [Display(Name = "User Name", Prompt = "Please enter email address")]
        [Required]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Please enter valid email address.")]
        public string EmailAddress { get; set; }

        [DataType("Password")]
        [Required]
        public string Password { get; set; }

        [NotMapped]
        [Required]
        [DataType("Password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}
