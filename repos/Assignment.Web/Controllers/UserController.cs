﻿using System;
using System.Net;
using Assignment.Services;
using Assignment.Services.ViewModel;
using Assignment.Web.DataAccess.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using reCAPTCHA.AspNetCore;

namespace Assignment.Web.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly IEmailService _emailService;
        private readonly IConfiguration _configuration;

        public UserController(IUserService userService, IEmailService emailService, IConfiguration configuration)
        {
            _userService = userService;
            _emailService = emailService;
            _configuration = configuration;
        }

        /// <summary>
        /// It redirects to the profile page of user
        /// </summary>
        /// <param name="userDetails">user details</param>
        /// <returns></returns>
        public IActionResult Welcome(UserDetails userDetails)
        {
            return View(userDetails);
        }

        /// <summary>
        /// It redirects to forgot password page
        /// </summary>
        /// <returns></returns>
        public IActionResult ForgotPassword()
        {
            return View();
        }

        /// <summary>
        /// User gets log out from any page
        /// </summary>
        /// <returns></returns>
        public IActionResult Logout()
        {
            HttpContext.Session.Remove("UserName");
            return RedirectToAction("Index", "Login");
        }

        /// <summary>
        /// It saves users data to db and validates all data on server side
        /// </summary>
        /// <param name="userDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateUser(UserDetails userDetails)
        {
            int userId = 0;
            RecaptchaResponse response = ValidateCaptcha(Request.Form["g-recaptcha-response"]);
            if (response.success)
            {
                if (userDetails != null && (!_userService.IsUserExist(userDetails.EmailAddress)))
                {
                    userDetails.ActivationLink = "https://localhost:44377/User/ResetPassword?userName=" + EncryptString(userDetails.EmailAddress);
                    userId = _userService.AddUser(userDetails);
                    if (userId > 0)
                    {
                        _emailService.EmailSend(new UserEmailViewModel()
                        {
                            ToEmail = userDetails.EmailAddress,
                            Name = userDetails.FirstName + " " + userDetails.LastName,
                            Subject = "Activation Link",
                            EmailContent = "<html><body><h1>Hi "+ userDetails.FirstName +", <br> Please activate your account using below link:</h1></br></br><a href='" + userDetails.ActivationLink + "'> Click me to Activate</a></body></html>"
                        });
                    }
                    ViewBag.UserName = userDetails.EmailAddress;
                    return View("ActivateLink");
                }
                else if (userDetails.IsActive)
                {
                    _userService.UpdateUser(userDetails);
                    return View("Welcome", userDetails);
                }
                else
                {

                    ModelState.AddModelError(string.Empty, "User already registered with this email address.");
                    return View("RegisterUser", userDetails);
                }

            }
            else
            {
                ModelState.AddModelError(string.Empty, "Please verify captcha.");
                return View("RegisterUser", userDetails);
            }
        }

        /// <summary>
        /// It redirects to reset password page through a link which sent on user email
        /// </summary>
        /// <param name="userName">encrypted UserName</param>
        /// <returns></returns>
        public IActionResult ResetPassword(string userName)
        {
            UserLogin userLogin = new UserLogin();
            userLogin.EmailAddress = DecryptString(userName);
            return View(userLogin);
        }

        /// <summary>
        /// Saves/updates user password 
        /// </summary>
        /// <param name="userLogin">User login details</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SavePassword(UserLogin userLogin)
        {
            if (_userService.IsUserExist(userLogin.EmailAddress))
            {
                var userDetails = _userService.GetUserDetails(userLogin.EmailAddress);
                userDetails.Password = EncryptString(userLogin.Password);
                userDetails.IsActive = true;
                userDetails.IsPasswordGenerated = true;
                _userService.UpdateUser(userDetails);
                return View("ConfirmationPassword");
            }
            else
            {
                ModelState.AddModelError(nameof(userLogin.EmailAddress), "User does not exist.");
                return View("ForgotPassword", userLogin);
            }
        }

        /// <summary>
        /// Method is used to encrypt input in base64 string
        /// </summary>
        /// <param name="input">String to be encrypt</param>
        /// <returns></returns>
        public static string EncryptString(string input)
        {
            try
            {
                byte[] encData_byte = new byte[input.Length];
                encData_byte = System.Text.Encoding.UTF8.GetBytes(input);
                string encodedData = Convert.ToBase64String(encData_byte);
                return encodedData;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in base64Encode" + ex.Message);
            }
        }

        /// <summary>
        /// Method to decrypt base 64 to normal text string
        /// </summary>
        /// <param name="encryptedString">string to be decrypt</param>
        /// <returns></returns>
        public static string DecryptString(string encryptedString)
        {
            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
            System.Text.Decoder utf8Decode = encoder.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(encryptedString);
            int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            string result = new String(decoded_char);
            return result;
        }

        /// <summary>
        /// This method is used to validate captcha
        /// </summary>
        /// <param name="response">Recaptch Response</param>
        /// <returns></returns>
        private RecaptchaResponse ValidateCaptcha(string response)
        {
            string secret = _configuration.GetValue<string>("RecaptchaSettings:SecretKey");
            var client = new WebClient();
            var jsonResult = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, response));
            return JsonConvert.DeserializeObject<RecaptchaResponse>(jsonResult.ToString());
        }

        /// <summary>
        /// Method to validate date of birth 
        /// </summary>
        /// <param name="DateOfBirth">User's Date of birth</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult IsValidDateOfBirth(DateTime DateOfBirth)
        {
            var min = DateTime.Now.AddYears(-80);
            var max = DateTime.Now.AddYears(-10);
            var msg = string.Format("Please enter a value between {0:MM/dd/yyyy} and {1:MM/dd/yyyy}", min, max);
            try
            {
                var date = DateOfBirth;
                if (date > max || date < min)
                    return Json(msg);
                else
                    return Json(true);
            }
            catch (Exception)
            {
                return Json(msg);
            }
        }

        /// <summary>
        /// It redirects to user registration page
        /// </summary>
        /// <returns></returns>
        public IActionResult RegisterUser()
        {
            return View();
        }

        /// <summary>
        /// Method to update user details
        /// </summary>
        /// <returns></returns>
        public IActionResult UpdateUser()
        {
            var userName = HttpContext.Session.GetString("UserName");
            if (string.IsNullOrWhiteSpace(userName))
                return RedirectToAction("Index", "Login");
            var userDetails = _userService.GetUserDetails(userName);
            return View("RegisterUser", userDetails);
        }

        /// <summary>
        /// Action to reset password when user clicks forgot passowrd
        /// </summary>
        /// <param name="userLogin"></param>
        /// <returns></returns>
        public IActionResult EmailSentNotification(UserLogin userLogin)
        {
            if (_userService.IsUserExist(userLogin.EmailAddress))
            {
                var userDetails = _userService.GetUserDetails(userLogin.EmailAddress);
                var resetLink = "https://localhost:44377/User/ResetPassword?userName=" + EncryptString(userDetails.EmailAddress);
                _emailService.EmailSend(new UserEmailViewModel()
                {
                    ToEmail = userDetails.EmailAddress,
                    Name = userDetails.FirstName + " " + userDetails.LastName,
                    Subject = "Reset Password",
                    EmailContent = "<html><body>Hi "+userDetails.FirstName+" ,</br>Please reset your password using below link:</br></br><a href='" + resetLink + "'> Click me to Reset</a></body></html>"
                });
                return View();
            }
            else
            {
                ModelState.AddModelError(nameof(userLogin.EmailAddress), "User does not exist.");
                return View("ForgotPassword", userLogin);
            }
        }

    }
}
