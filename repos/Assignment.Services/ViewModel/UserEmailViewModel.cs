﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment.Services.ViewModel
{
    public class UserEmailViewModel
    {
        public string Name { get; set; }
        public string TextPartType { get; set; }
        public string EmailContent { get; set; }
        public string ToEmail { get; set; }
        public string Subject { get; set; }
    }
}
