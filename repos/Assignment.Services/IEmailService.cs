﻿using Assignment.Services.ViewModel;

namespace Assignment.Services
{
    public interface IEmailService
    {
        void EmailSend(UserEmailViewModel userEmailViewModel);
    }
}
