﻿
using Assignment.Web.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Assignment.Web
{
    public class EntityDbContext : DbContext
    {
        private readonly IConfiguration _config;

        public EntityDbContext(IConfiguration config)
        {
            _config = config;
        }
        public DbSet<UserDetails> UserDetails { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_config.GetValue<string>("Database:connection"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserDetails>()
                .HasKey(b => b.EmailAddress)
                .HasName("PK_UserDetails");

            modelBuilder.Entity<UserLogin>().HasNoKey();
        }

    }

}
