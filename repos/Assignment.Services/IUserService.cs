﻿using Assignment.Web.DataAccess.Models;

namespace Assignment.Services
{
    public interface IUserService
    {
        int AddUser(UserDetails user);
        bool IsUserExist(string userName);
        bool IsUserActive(string userName);
        UserDetails AuthenticateUser(string userName, string password);
        UserDetails GetUserDetails(string userName);
        void UpdateUser(UserDetails user);
    }
}
