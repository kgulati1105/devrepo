﻿using Assignment.Services.ViewModel;
using Microsoft.Extensions.Configuration;
using MimeKit;

namespace Assignment.Services.Impl
{
    public class EmailService : IEmailService
    {
     private readonly IConfiguration _configuration;
      public EmailService(IConfiguration configuration)
        {
            _configuration = configuration;
        }  
        public void EmailSend(UserEmailViewModel userEmailViewModel)
        {
            var message = new MimeMessage();
            var fromEmail = _configuration.GetValue<string>("EmailConfig:FromEmail");
            var password = _configuration.GetValue<string>("EmailConfig:Password");
            message.From.Add(new MailboxAddress("Admin", fromEmail));
            message.To.Add(new MailboxAddress(userEmailViewModel.Name, userEmailViewModel.ToEmail));
            message.Subject = userEmailViewModel.Subject;
            message.Body = new TextPart("Html")
            {
                Text = userEmailViewModel.EmailContent
            };

            using (var client = new MailKit.Net.Smtp.SmtpClient())
            {

                client.Connect("smtp.gmail.com", 587, false);

                //SMTP server authentication if needed
                client.Authenticate(fromEmail, password);

                client.Send(message);

                client.Disconnect(true);
            }
        }
    }
}
