﻿using Assignment.Web.DataAccess.Models;
using System.Collections.Generic;
using System.Linq;


namespace Assignment.Web.DataAccess
{
    public class Repository
    {
        private EntityDbContext _context { get; set; }

        public Repository(EntityDbContext context)
        {
            _context = context;
        }

        public int Add(UserDetails userDetails)
        {
            _context.Add(userDetails);
            return _context.SaveChanges();
        }

        public UserDetails Get(string emailAddress)
        {
            return _context.UserDetails.FirstOrDefault(e => e.EmailAddress == emailAddress);
        }

        public void Update(UserDetails userDetails)
        {
            _context.Update(userDetails);
            _context.SaveChanges();
        }

        public IEnumerable<UserDetails> GetAll()
        {
            return _context.UserDetails.ToList<UserDetails>();
        }

        public UserDetails AuthenticateUser(string userName, string password)
        {
            var user = _context.UserDetails.Where(a => a.EmailAddress.Equals(userName) && a.Password.Equals(password)).FirstOrDefault();
            return user;
        }

        public bool CheckIsActiveUser(string userName)
        {
            return _context.UserDetails.Any(a => a.EmailAddress.Equals(userName) && a.IsActive == true);
        }

        public bool DoesUserExist(string userName)
        {
            return _context.UserDetails.Any(a => a.EmailAddress.Equals(userName));
        }
    }
}
