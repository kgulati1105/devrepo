﻿using Assignment.Web.DataAccess;
using Assignment.Web.DataAccess.Models;

namespace Assignment.Services.Impl
{
    public class UserService : IUserService
    {
        private readonly Repository _repository;
        public UserService(Repository repository)
        {
            _repository = repository;
        }

        public int AddUser(UserDetails user)
        {
            return _repository.Add(user);
        }

        public bool IsUserExist(string userName)
        {
            return _repository.DoesUserExist(userName);
        }

        public bool IsUserActive(string userName)
        {
            return _repository.CheckIsActiveUser(userName);
        }

        public UserDetails AuthenticateUser(string userName, string password)
        {
            return _repository.AuthenticateUser(userName, password);
        }

        public UserDetails GetUserDetails(string userName)
        {
            return _repository.Get(userName);
        }

        public void UpdateUser(UserDetails user)
        {
            _repository.Update(user);
        }
    }
}
